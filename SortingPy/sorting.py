from collections import defaultdict
import cmd
import random
import sys

split_steps = defaultdict(list)
merge_steps = defaultdict(list)


def reset_steps():
    "очитска списков"
    split_steps.clear()
    merge_steps.clear()


def mergesort(arr, n=1):
    "алгоритм сортировки слиянием"
    size = len(arr)

    if n == 1:
        reset_steps()

    if size == 1:
        return arr

    split_steps[n].append(arr[:size // 2])
    split_steps[n].append(arr[size // 2:])

    l_arr = mergesort(arr[:size // 2], n + 1)
    r_arr = mergesort(arr[size // 2:], n + 1)

    l_size = len(l_arr)
    r_size = len(r_arr)

    i, j = 0, 0

    merged = []

    while i < l_size and j < r_size:
        if l_arr[i] < r_arr[j]:
            merged.append(l_arr[i])
            i += 1
        else:
            merged.append(r_arr[j])
            j += 1

    merged.extend(l_arr[i:])
    merged.extend(r_arr[j:])

    merge_steps[n].append(merged)

    return merged


def get_split_steps():
    "шаг разбиения"
    return split_steps


def get_merge_steps():
    "шаг слияния"
    return merge_steps

class ColorCode:
    CGREEN = '\33[32m'
    CBLUE = '\33[34m'
    CYELLOW = '\33[33m'
    CEND = '\033[0m'


def colored_text(text, color):
    if color in ('green', 'Green', 'GREEN'):
        return ColorCode.CGREEN + text + ColorCode.CEND
    elif color in ('blue', 'Blue', 'BLUE'):
        return ColorCode.CBLUE + text + ColorCode.CEND
    elif color in ('yellow', 'Yellow', 'YELLOW'):
        return ColorCode.CYELLOW + text + ColorCode.CEND


class SortingShell(cmd.Cmd):
    "Командная строка."

    intro = '''
\nДобро пожаловать в сортировчную командную строку :D \n
В данный момент реализована только сортировка слиянием.\n
Имеется 3 команды: \n
create <длина списка> - создание int списка заданной длины(от 2 до 100) \n
mergesort - сортировка слиянием заданного массива \n
exit - ну вы поняли\n'''
    prompt = colored_text('>>>', 'BLUE')
    file = None

    _list = []

    def emptyline(self, *args):
        pass

    def do_create(self, *args):
        "Создание случайного списка заданной длины (2 to 100)"

        size = args[0]

        if not size.isdigit():
            print('Используй : create <длина списка (от 2 to 100)>')
        else:
            size = int(size)

            if size > 100 or size < 2:
                print('Диапазон значений должен быть от 2 до 100')
            else:
                self._list = [random.randint(1, 1 << 8) for i in range(size)]
                print('Созданый список:', self._list)


    def do_mergesort(self, *args):
        "Сортировка списка методом сортировки слиянием"

        if len(self._list) == 0:
            print('Ты должен создать список "create" ')
        else:
            print('Original list :', self._list, '\n')
            sorted_list = mergesort(self._list)

            for i, split_step in get_split_steps().items():
                print('Split step {0}'.format(i).ljust(13), ':',
                    colored_text(' | ', 'BLUE').join([str(e) for e in split_step]))
            else:
                print()

            num_merge_steps = len(get_merge_steps().items())

            for i, merge_step in sorted(get_merge_steps().items(), reverse=True):
                print('Merge step {0}'.format(num_merge_steps - i + 1).ljust(13), ':',
                    colored_text(' | ', 'BLUE').join([str(e) for e in merge_step]))
            else:
                print()

            print('Sorted list   :', sorted_list)

    def do_exit(self, *args):
        "Функция 'выход' ."

        print('Спасибо!')

        return True


if __name__ == '__main__':
    SortingShell().cmdloop()